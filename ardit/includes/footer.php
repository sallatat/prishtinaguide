<section id="footer_top_area">
	<div class="fix footer_top center">
		<div class="fix footer_top_container">
			<div class="fix single_footer_top floatleft">
				<h2><span>Recent posts</span></h2>
				<ul>
					<li><a href="">single footer recent post</a> (10)</li>
					<li><a href="">single footer recent post</a> (10)</li>
					<li><a href="">single footer recent post</a> (10)</li>
					<li><a href="">single footer recent post</a> (10)</li>
					<li><a href="">single footer recent post</a> (10)</li>
				</ul>
			</div>
			<div class="fix single_footer_top floatleft">
				<h2><span>About Prishtina Guide</span></h2>
				<p>onec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
				<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Donec id elit non mi porta gravida at eget metus.</p>

			</div>
			<div class="fix single_footer_top floatleft">
				<h2><span>Comments</span></h2>
				<ul>
					<li>Orman Clark on <a href="">Sample Post </a>With<a href=""> Threaded Comments</a></li>
					<li>Orman Clark on <a href="">Sample Post </a>With<a href=""> Threaded Comments</a></li>
					<li>Orman Clark on <a href="">Sample Post </a>With<a href=""> Threaded Comments</a></li>
				</ul>
			</div>

		</div>


	</div>
</section>
<section id="footer_bottom_area">
	<div class="fix footer_bottom center">
		<div class="w3-container w3-center  w3-margin-bottom">
			<h5>Find Us On</h5>
			<div class="w3-xlarge w3-padding-16">
				<i class="fa fa-facebook-official w3-hover-opacity"></i>
				<i class="fa fa-instagram w3-hover-opacity"></i>
				<i class="fa fa-snapchat w3-hover-opacity"></i>
				<i class="fa fa-pinterest-p w3-hover-opacity"></i>
				<i class="fa fa-twitter w3-hover-opacity"></i>
				<i class="fa fa-linkedin w3-hover-opacity"></i>
			</div>
		</div>
	</div>
</section>
<script src="http://code.jquery.com/jquery.js"></script>
<script type="text/javascript" src="js/selectnav.min.js"></script>
<script type="text/javascript" src="js/script.js"> </script>	
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>

</html>