<?php
session_start();
try {
    $db = new PDO('mysql:host=localhost;dbname=prishtinaguide', 'root', '');
    $db ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $page = isset($_GET['p'])?$_GET['p']:'';
}  
catch(PDOException $e){
    echo $e->getMessage();
    die();
}

if($page == 'add') {
	$fName = $_POST['fName'];
	$lName = $_POST['lName'];
	$username = $_POST['username'];
	$password = $_POST['password'];
	$age = $_POST['age'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	$role = $_POST['role'];

	$sql_users = "INSERT INTO users VALUES(NULL, '$fName', '$lName', '$age', '$email', '$phone', '$username', '$password');";
	$db->query($sql_users);

    $lastID = $db->lastInsertId();
    $user_roles = "INSERT INTO user_roles VALUES(NULL, '$lastID', '$role')";
    $db->query($user_roles);
}

else if($page=='edit') {
    $id = $_POST['id'];
    $fName = $_POST['fName'];
    $lName = $_POST['lName'];
    $username = $_POST['username'];
    $password = $_POST['password'];
    $age = $_POST['age'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $role = $_POST['role'];
    
    $sql_users = "UPDATE users SET firstName = '$fName', lastName = '$lName', age = '$age', email = '$email',
                 contactNo = '$phone', username = '$username', password = '$password' WHERE userID = '$id'";
    $db->query($sql_users);

    //update users ROLE (not working for now :P)
    /*$userRoles = "UPDATE user_roles SET roleID = '$role' WHERE userRolesID = '$id')";
    $db->query($userRoles);*/
}

else if($page=='del') {
    $id = $_GET['id'];

    $userRoles = "DELETE FROM user_roles WHERE userRolesID = $id";
    $db->query($userRoles);

    $users = "DELETE FROM users WHERE userID = $id";
    $db->query($users);
}

else if($page == 'select_id') {
    $id = $_POST['id'];
    $stmt = $db->prepare("SELECT *, user_roles.roleID 
                          FROM `users` u 
                          INNER JOIN user_roles ON u.userID = user_roles.userID 
                          WHERE u.userID = '$id'");
    $stmt->execute();
    $row = $stmt->fetch();

    //echo $row['userID'];

    echo $row['firstName'];
    echo " || ";
    echo $row['lastName'];
    echo " || ";
    echo $row['username'];
    echo " || ";
    echo $row['password'];
    echo " || ";
    echo $row['email'];
    echo " || ";
    echo $row['age'];
    echo " || ";
    echo $row['contactNo'];
    echo " || ";
    echo $row['roleID'];
}

else if ($page == 'isReg') {
    $username = $_POST['user'];
    $password = $_POST['pass'];

    $stmt = $db->prepare("SELECT username FROM users WHERE username = '$username' AND password = '$password'");
    $stmt->execute();
    $row = $stmt->fetch();

    if($row == 0) {
        echo 'null';
    }
    else {
        $_SESSION['USERNAME'] = $row['username'];
        echo $row['username'];
        echo " || ";
        echo $row['password'];
    }
}

else if ($page == 'checkUsername') {
    $username = $_POST['user'];

    $stmt = $db->prepare("SELECT username FROM users WHERE username = '$username'");
    $stmt->execute();
    $row = $stmt->fetch();

    if($row == 0) {
        echo 'null';
    }
    else {
        echo $row['username'];
    }
}

else {
	    //$stmt = $db->prepare("SELECT * FROM users ORDER BY userID");
    $stmt = $db->prepare("SELECT users.userID, users.firstName, users.lastName, users.age, users.email, 
                          users.contactNo, users.username, users.password, user_roles.roleID
                          FROM users
                          INNER JOIN user_roles ON users.userID = user_roles.userID;");
    $stmt->execute();

    while($row = $stmt->fetch()) {
        $id = $row['userID'];
        $fname = $row['firstName'];
        $lname = $row['lastName'];
        $user = $row['username'];
        $pass = $row['password'];
        $age = $row['age'];
        $email = $row['email'];
        $contactNo = $row['contactNo'];
        ?>
        <tr>
            <td><?php echo $fname ?></td>
            <td><?php echo $lname ?></td>
            <td><?php echo $user ?></td>
            <td><?php echo $pass ?></td>
            <td><?php echo $age ?></td>
            <td><?php echo $email ?></td>
            <td><?php echo $contactNo ?></td>
            <td>
                <?php
                    if($row['roleID'] == 1) { echo "<p><span class='label label-warning'>Admin</span></p>"; }
                    else if($row['roleID'] == 2) { echo "<p><span class='label label-default'>User</span></p>"; }
                ?>
            </td>
            <td>
            <button class="btn btn-warning btn-sm" onclick="editMode('<?php echo $id; ?>')">Edit</button>
                <button class="btn btn-danger btn-sm" onclick="deleteData('<?php echo $id; ?>')">Delete</button>
            </td>
        </tr>
        <?php
    }
}

?>