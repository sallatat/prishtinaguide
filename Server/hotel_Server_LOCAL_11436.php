<?php

$db = new PDO('mysql:host=localhost;dbname=prishtinaguide', 'root', '');
$page = isset($_GET['p'])?$_GET['p']:'';

if($page=='add'){
    $hotel = $_POST['ht'];
    $address = $_POST['ad'];
    $desc = $_POST['de'];
    $phone = $_POST['ph'];
    $rate_btn = $_POST['rate_btn'];
    $price = $_POST['price'];
    $stmt = $db->prepare("INSERT INTO hoteltype VALUES('', ?, ?, ?)");
    $stmt->bindParam(1, $phone);
    $stmt->bindParam(2, $price);
    $stmt->bindParam(3, $rate_btn);
    $stmt->execute();
    $row = $stmt->fetch_assoc();
    $hotelTypeID = $row['hotelTypeID'];
    $stmt = $db->prepare("INSERT INTO hotels VALUES('', ?, ?, ?, ?, ?)");
    $stmt->bindParam(1, $hotel);
    $stmt->bindParam(2, $address);
    $stmt->bindParam(3, $desc);
    $stmt->bindParam(4, 1);
    $stmt->bindParam(5, $hotelTypeID);
    $stmt->execute();
}

else if($page=='edit') {
    $id = $_POST['id'];
    $hotel = $_POST['ht'];
    $address = $_POST['ad'];
    $desc = $_POST['de'];
    $phone = $_POST['ph'];
    $rate_btn = $_POST['rate_btn'];
    $stmt = $db->prepare("UPDATE hotels SET Emri=?, Lokacioni=?, Pershkrimi=?, Kontakti=?, Vleresimi=? WHERE id=?");
    $stmt->bindParam(1, $hotel);
    $stmt->bindParam(2, $address);
    $stmt->bindParam(3, $desc);
    $stmt->bindParam(4, $phone);
    $stmt->bindParam(5, $rate_btn);
    $stmt->bindParam(6, $id);
    if($stmt->execute())
        echo 'Success update data';
    else
        echo 'Fail update data';
}

else if($page=='del') {
    $id = $_GET['id'];
    $stmt = $db->prepare("DELETE FROM hotels WHERE id=?");
    $stmt->bindParam(1, $id);
    if($stmt->execute()) 
        echo "Success delete data";
    else
        echo "Fail delete data";
}

else {
    $stmt = $db->prepare("SELECT * FROM hotels ORDER BY Emri");
    $stmt->execute();
    while($row = $stmt->fetch()) {
        ?>
        <tr>
            <td><?php echo $row['Emri'] ?></td>
            <td><?php echo $row['Lokacioni'] ?></td>
            <td><?php echo $row['Pershkrimi'] ?></td>
            <td><?php echo $row['Kontakti'] ?></td>
            <td><?php echo $row['Vleresimi'] ?></td>
            <td>
                <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#edit-<?php echo $row['id'] ?>">Edit</button>
                <!-- Modal -->
                <div class="modal fade bs-example-modal-sm" id="edit-<?php echo $row['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="editLabel-<?php echo $row['id'] ?>">
                  <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" style="font-weight: bold;" id="editLabel-<?php echo $row['id'] ?>">Edit <?php echo $row['Emri']; ?></h4>
                      </div>
                        <form>  
                        <div class="modal-body">

                            <input type="hidden" id="<?php echo $row['id'] ?>" value="<?php echo $row['id'] ?>">

                            <div  class="form-group form-group-sm">
                                <label for="ht">Hotel</label>
                                <input type="text" class="form-control" id="ht-<?php echo $row['id'] ?>" value="<?php echo $row['Emri'] ?>">
                            </div>
                            <div  class="form-group form-group-sm">
                              <label for="ad">Address</label>
                              <input type="text" class="form-control" id="ad-<?php echo $row['id'] ?>" value="<?php echo $row['Lokacioni'] ?>">
                            </div>
                            <div class="form-group form-group-sm">
                              <label for="de">Description</label>
                              <textarea class="form-control" rows="5" id="de-<?php echo $row['id'] ?>" placeholder="Description"><?php echo $row['Pershkrimi'] ?></textarea>
                            </div>
                            <div  class="form-group form-group-sm">
                              <label for="ph">Phone</label>
                              <input type="number" class="form-control" id="ph-<?php echo $row['id'] ?>" value="<?php echo $row['Kontakti'] ?>">
                            </div>
                            <div class="form-group form-group-sm">
                                <label>Rate ⭐</label>
                                <select id="rate_btn-<?php echo $row['id'] ?>" value="<?php echo $row['Vleresimi'] ?>" class="selectpicker btn btn-sm pull-right">
                                    <option style="font-size: 14px;" value="1.0">1.0</option>
                                    <option style="font-size: 14px;" value="2.0">2.0</option>
                                    <option style="font-size: 14px;" value="3.0">3.0</option>
                                    <option style="font-size: 14px;" value="4.0">4.0</option>
                                    <option style="font-size: 14px;" value="5.0">5.0</option>
                                </select>
                            </div>

                        </div>

                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" onclick="hotel_updateData(<?php echo $row['id'] ?>)" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                    </div>
                  </div>
                </div>
                <button onclick="hotel_deleteData(<?php echo $row['id'] ?>)" class="btn btn-danger btn-sm">Delete</button>
            </td>
        </tr>
        <?php
    }
}

?>