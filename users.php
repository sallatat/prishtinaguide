<?php 
  session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>PrishtinaGuide - Add User</title>

<<<<<<< HEAD
  <!-- Bootstrap -->
  <!--<link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body onload="onLoad()">
=======
</head>
<body onload="load()">
>>>>>>> origin/pg-01
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="js/users.js"></script>
<<<<<<< HEAD
=======
<script src="js/global.js"></script>
>>>>>>> origin/pg-01

    
<!-- Design -->
<div class="container" id="main">

	<div id="globalBtns" class="btn-group btn-group-sm pull-right" role="group" aria-label="...">
    <?php
        if(empty($_SESSION['USERNAME'])) {
    ?>
          <?
          ob_start();
          ?>
          <button id="adminBtn" type="button" class="btn btn-default btn_color" onclick="window.location='loginForm.php'"><span class="glyphicon glyphicon-user"></span><span style="color: black; margin-left: 5px">Login </span></button>
          <?
          echo ob_get_clean();
          ?>
    <?php
        }
        else {
    ?>    <?
          ob_start();
          ?>
          <button id="adminBtn" type="button" class="btn btn-default btn_color" onclick="window.location='admin_crud.php'"><span class="glyphicon glyphicon-user"></span><span style="color: limegreen; margin-left: 5px"> <?php echo $_SESSION["USERNAME"] ?> </span></button>
<<<<<<< HEAD
          <?
          echo ob_get_clean();
          ?>
    <?php
        }
    ?>

    <?php
        if(!empty($_SESSION['USERNAME'])) {
    ?>
          <?
          ob_start();
          ?>
          <button id="logoutBtn" type="button" onclick="window.location='loginForm.php'" class="btn btn-default btn_color"><span style="color: red">Logout </span></button>
=======
          <button id="logoutBtn" type="button"  class="btn btn-default btn_color"><span style="color: red">Logout </span></button>
>>>>>>> origin/pg-01
          <?
          echo ob_get_clean();
          ?>
    <?php
        }
    ?>
    <button id="usersBtn" type="button" onclick="window.location='users.php'" class="btn btn-default btn_color">Users</button>
	  <button id="hotels" type="button" onclick="window.location='addHotel.php'" class="btn btn-default btn_color">Hotels</button>
	  <button id="houses" type="button" onclick="window.location='addHouse.php'" class="btn btn-default btn_color">Houses</button>
	</div>

    <h2>Add User</h2>
    <hr>
    
<div class="row">
    
<form id="ff">
    
<ul class="pull-right" style="list-style-type: none;">
  <li><button type="button" id="saveBtn" onclick="isValidStage_1('save')" class="btn btn-block btn-success pull-right">Save</button></li>
  <li><button type="button" id="updateBtn" onclick="isValidStage_1('update')" class="btn btn-block btn-warning pull-right">Update</button></li>
  <li><button type="button" id="cancelBtn" onclick="cancelButton()" class="btn btn-block btn-danger pull-right">Cancel</button></li>
</ul>
    
<div class="col-sm-3">
    <div class="form-group form-group-sm">
        <label for="fName" >First name:</label>
        <input type="text" class="form-control" id="fName" placeholder="Required" required>
    </div>
    <div class="form-group form-group-sm">
      <label for="lName">Last name:</label>
      <input type="text" class="form-control" id="lName" placeholder="Required" required>
    </div>
    <div id="usernameDiv" class="form-group form-group-sm">
      <label for="username">Username:</label>
      <input type="text" class="form-control" id="username" placeholder="Required" required>
      <label class="control-label" for="username" style="font-size: 12px;visibility: hidden">This username exists. Please try other usernames!</label>
    </div>
</div>

<div class="col-sm-3">
    <div class="form-group form-group-sm">
      <label for="password">Password:</label>
      <input type="text" class="form-control" id="password" placeholder="Required" required>
    </div>
    <div class="form-group form-group-sm">
      <label for="email">Email:</label>
      <input type="email" class="form-control" id="email" placeholder="Required" required>
    </div>
    <div class="form-group form-group-sm">
      <label for="age">Age:</label>
      <input type="number" class="form-control" id="age" placeholder="Required" required>
    </div>
</div>

<div class="col-sm-3">
    <div class="form-group form-group-sm">
      <label for="phone">Phone:</label>
      <input type="number" class="form-control" id="phone" placeholder="Required" required>
    </div>
    <div class="form-group form-group-sm">
        <label for="">Role:</label>
        <div id="bb" class="btn-group btn-block btn-group-sm" data-toggle="buttons">
            <label id="option1" class="btn btn-default btn-secondary" onclick="adminBtnCheck()">
                <input id="adm" type="radio" value="1" autocomplete="off"> Administrator
            </label>
            <label id="option2" class="btn btn-default btn-warning active" onclick="usersBtnCheck()">
                <input id="usr" type="radio" value="2" autocomplete="off" checked> User
            </label>
        </div>
    </div>

</div>
</form>
    

<!-- Bootstrap Table -->
<div style="top: 30px;" class="col-sm-12">
<hr>
<input type="text" class="pull-right" id="search" onkeyup="searchBox()" placeholder="Search for names..">
<h3>Table</h3>
<table id="myTable" class="table table-bordered table-striped table-condensed table-sm">
    <thead class="table-inverse">
        <tr>
            <th width="100">First name</th>
            <th width="100">Last name</th>
            <th width="100">Username</th>
            <th width="100">Password</th>
            <th width="100">Age</th>
            <th width="180">Email</th>
            <th width="100">Phone</th>
            <th width="100">Role</th>
            <th width="130">Actions</th>
        </tr>
    </thead>

    <tbody>
    </tbody>
</table>
</div>
    
</div>
</div>
</body>
</html>