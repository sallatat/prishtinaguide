<?php

$db = new PDO('mysql:host=localhost;dbname=prishtinaguide', 'root', '');
$page = isset($_GET['p'])?$_GET['p']:'';

if($page=='add'){
    $owner = $_POST['owner_input'];
    $address = $_POST['address_input'];
    $desc = $_POST['desc_input'];
    $phone = $_POST['phone_input'];
    $price = $_POST['price_input'];
    $stmt = $db->prepare("INSERT INTO houses VALUES('', ?, ?, ?, ?, ?)");
    $stmt->bindParam(1, $owner);
    $stmt->bindParam(2, $address);
    $stmt->bindParam(3, $desc);
    $stmt->bindParam(4, $phone);
    $stmt->bindParam(5, $price);
    if($stmt->execute())
        echo 'Success add data';
    else
        echo 'Fail add data';
}

else if($page=='edit') {
    $id = $_POST['id'];
    $owner = $_POST['owner_input'];
    $address = $_POST['address_input'];
    $desc = $_POST['desc_input'];
    $phone = $_POST['phone_input'];
    $price = $_POST['price_input'];
    $stmt = $db->prepare("UPDATE houses SET pronari=?, adresa=?, pershkrimi=?, kontakti=?, cmimi=? WHERE id=?");
    $stmt->bindParam(1, $owner);
    $stmt->bindParam(2, $address);
    $stmt->bindParam(3, $desc);
    $stmt->bindParam(4, $phone);
    $stmt->bindParam(5, $price);
    $stmt->bindParam(6, $id);
    if($stmt->execute())
        echo 'Success update data';
    else
        echo 'Fail update data';
}

else if($page=='del') {
    $id = $_GET['id'];
    $stmt = $db->prepare("DELETE FROM houses WHERE id=?");
    $stmt->bindParam(1, $id);
    if($stmt->execute()) 
        echo "Success delete data";
    else
        echo "Fail delete data";
}

else {
    $stmt = $db->prepare("SELECT * FROM houses ORDER BY pronari");
    $stmt->execute();
    while($row = $stmt->fetch()) {
        ?>
        <tr>
            <td><?php echo $row['pronari'] ?></td>
            <td><?php echo $row['adresa'] ?></td>
            <td><?php echo $row['pershkrimi'] ?></td>
            <td><?php echo $row['kontakti'] ?></td>
            <td><?php echo $row['cmimi'] ?></td>
            <td>
                <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#edit-<?php echo $row['id'] ?>">Edit</button>
                <!-- Modal -->
                <div class="modal fade bs-example-modal-sm" id="edit-<?php echo $row['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="editLabel-<?php echo $row['id'] ?>">
                  <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" style="font-weight: bold;" id="editLabel-<?php echo $row['id'] ?>">Edit</h4>
                      </div>
                        <form>  
                        <div class="modal-body">

                            <input type="hidden" id="<?php echo $row['id'] ?>" value="<?php echo $row['id'] ?>">

                            <div  class="form-group form-group-sm">
                                <label for="owner_input">Owner</label>
                                <input type="text" class="form-control" id="owner_input-<?php echo $row['id'] ?>" value="<?php echo $row['owner'] ?>">
                            </div>
                            <div  class="form-group form-group-sm">
                              <label for="address_input">Address</label>
                              <input type="text" class="form-control" id="address_input-<?php echo $row['id'] ?>" value="<?php echo $row['adresa'] ?>">
                            </div>
                            <div class="form-group form-group-sm">
                              <label for="desc_input">Description</label>
                              <textarea class="form-control" rows="5" id="desc_input-<?php echo $row['id'] ?>" placeholder="Description"><?php echo $row['pershkrimi'] ?></textarea>
                            </div>
                            <div  class="form-group form-group-sm">
                              <label for="phone_input">Phone</label>
                              <input type="number" class="form-control" id="phone_input-<?php echo $row['id'] ?>" value="<?php echo $row['kontakti'] ?>">
                            </div>
                            <div  class="form-group form-group-sm">
                              <label for="price_input">Price</label>
                              <input type="number" class="form-control" id="price_input-<?php echo $row['id'] ?>" value="<?php echo $row['cmimi'] ?>">
                            </div>

                        </div>

                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" onclick="house_updateData(<?php echo $row['id'] ?>)" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                    </div>
                  </div>
                </div>
                <button onclick="house_deleteData(<?php echo $row['id'] ?>)" class="btn btn-danger btn-sm">Delete</button>
            </td>
        </tr>
        <?php
    }
}

?>