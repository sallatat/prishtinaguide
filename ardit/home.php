<?php include "includes/dbconnection.php" ?>
<?php include "includes/header.php" ?>

<div class="fix main_content_area">
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <div class="item active">
        <img src="Images/travel/01.jpg" alt="Los Angeles" style="width:100%;">
        <div class="carousel-caption">
          <h3>Los Angeles</h3>
          <p>LA is always so much fun!</p>
        </div>
      </div>

      <div class="item">
        <img src="Images/travel/02.jpg" alt="Chicago" style="width:100%;">
        <div class="carousel-caption">
          <h3>Chicago</h3>
          <p>Thank you, Chicago!</p>
        </div>
      </div>
    
      <div class="item">
        <img src="Images/travel/03.jpg" alt="New York" style="width:100%;">
        <div class="carousel-caption">
          <h3>New York</h3>
          <p>We love the Big Apple!</p>
        </div>
      </div>
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev" onclick="$('#myCarousel').carousel('prev')">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next" onclick="$('#myCarousel').carousel('next')">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
	<div class="fix home_main_content" id="home_main_content">
		<div class="main_content floatleft">
			<div class="fix main_content_container">

				<div class="fix single_content floatleft">
					<h2><span>Technology<span></h2>
					<div class="fix single_content_feature">
						<a href=""><img src="images/feature.png" alt=""/></a>
						<h3><a href="">Silicon Valley Shows Signs of Dot-Com Frenzy Silicon Valley Sho,Dot-Com Frenzy Silicon Valley Sho</a></h3>
						<span>August 4 2010, <a href="">8 Comments</a></span>
						<p>Lorem ipsum ex vix illud naaconummy, novum tation et his. At vix scripta patrioque scribentur...</p>
					</div>

					<ul id="post_list">
						<li>
							<div class="fix single_home_content">
								<div class="fix single_home_content_img">
									<img src="images/small_feature.png" alt="" />
								</div>
								<div class="single_home_content_title">
									<h4><a href="">Fly Fishers Serving as Transports for Noxious Little Invaders</a></h4>
									<p>August 4 2010, <a href="">8 Comments</a></p>
								</div>
							</div>
						</li>
						<li>
							<div class="fix single_home_content">
								<div class="fix single_home_content_img">
									<img src="images/small_feature.png" alt="" />
								</div>
								<div class="single_home_content_title">
									<h4><a href="">Fly Fishers Serving as Transports for Noxious Little Invaders</a></h4>
									<p>August 4 2010, <a href="">8 Comments</a></p>
								</div>
							</div>
						</li>
						<li>
							<div class="fix single_home_content">
								<div class="fix single_home_content_img">
									<img src="images/small_feature.png" alt="" />
								</div>
								<div class="single_home_content_title">
									<h4><a href="">Fly Fishers Serving as Transports for Noxious Little Invaders</a></h4>
									<p>August 4 2010, <a href="">8 Comments</a></p>
								</div>
							</div>
						</li>
					</ul>

				</div>

				<!-- Clean template By wpfreeware.com -->

				<div class="fix single_content floatleft">
					<h2><span>Technology<span></h2>
					<div class="fix single_content_feature">
						<a href=""><img src="images/feature.png" alt=""/></a>
						<h3><a href="">Silicon Valley Shows Signs of Dot-Com Frenzy Silicon Valley Sho,Dot-Com Frenzy Silicon Valley Sho</a></h3>
						<span>August 4 2010, <a href="">8 Comments</a></span>
						<p>Lorem ipsum ex vix illud naaconummy, novum tation et his. At vix scripta patrioque scribentur...</p>
					</div>

					<ul id="post_list">
						<li>
							<div class="fix single_home_content">
								<div class="fix single_home_content_img">
									<img src="images/small_feature.png" alt="" />
								</div>
								<div class="single_home_content_title">
									<h4>Fly Fishers Serving as Transports for Noxious Little Invaders</h4>
									<p>August 4 2010, <a href="">8 Comments</a></p>
								</div>
							</div>
						</li>
						<li>
							<div class="fix single_home_content">
								<div class="fix single_home_content_img">
									<img src="images/small_feature.png" alt="" />
								</div>
								<div class="single_home_content_title">
									<h4>Fly Fishers Serving as Transports for Noxious Little Invaders</h4>
									<p>August 4 2010, <a href="">8 Comments</a></p>
								</div>
							</div>
						</li>
						<li>
							<div class="fix single_home_content">
								<div class="fix single_home_content_img">
									<img src="images/small_feature.png" alt="" />
								</div>
								<div class="single_home_content_title">
									<h4>Fly Fishers Serving as Transports for Noxious Little Invaders</h4>
									<p>August 4 2010, <a href="">8 Comments</a></p>
								</div>
							</div>
						</li>
					</ul>

				</div>

				<div class="fix single_content floatleft">
					<h2><span>Technology<span></h2>
					<div class="fix single_content_feature">
						<a href=""><img src="images/feature.png" alt=""/></a>
						<h3><a href="">Silicon Valley Shows Signs of Dot-Com Frenzy Silicon Valley Sho,Dot-Com Frenzy Silicon Valley Sho</a></h3>
						<span>August 4 2010, <a href="">8 Comments</a></span>
						<p>Lorem ipsum ex vix illud naaconummy, novum tation et his. At vix scripta patrioque scribentur...</p>
					</div>

					<ul id="post_list">
						<li>
							<div class="fix single_home_content">
								<div class="fix single_home_content_img">
									<img src="images/small_feature.png" alt="" />
								</div>
								<div class="single_home_content_title">
									<h4>Fly Fishers Serving as Transports for Noxious Little Invaders</h4>
									<p>August 4 2010, <a href="">8 Comments</a></p>
								</div>
							</div>
						</li>
						<li>
							<div class="fix single_home_content">
								<div class="fix single_home_content_img">
									<img src="images/small_feature.png" alt="" />
								</div>
								<div class="single_home_content_title">
									<h4>Fly Fishers Serving as Transports for Noxious Little Invaders</h4>
									<p>August 4 2010, <a href="">8 Comments</a></p>
								</div>
							</div>
						</li>
						<li>
							<div class="fix single_home_content">
								<div class="fix single_home_content_img">
									<img src="images/small_feature.png" alt="" />
								</div>
								<div class="single_home_content_title">
									<h4>Fly Fishers Serving as Transports for Noxious Little Invaders</h4>
									<p>August 4 2010, <a href="">8 Comments</a></p>
								</div>
							</div>
						</li>
					</ul>

				</div>

				<div class="fix single_content floatleft">
					<h2><span>Technology<span></h2>
					<div class="fix single_content_feature">
						<a href=""><img src="images/feature.png" alt=""/></a>
						<h3><a href="">Silicon Valley Shows Signs of Dot-Com Frenzy Silicon Valley Sho,Dot-Com Frenzy Silicon Valley Sho</a></h3>
						<span>August 4 2010, <a href="">8 Comments</a></span>
						<p>Lorem ipsum ex vix illud naaconummy, novum tation et his. At vix scripta patrioque scribentur...</p>
					</div>

					<ul id="post_list">
						<li>
							<div class="fix single_home_content">
								<div class="fix single_home_content_img">
									<img src="images/small_feature.png" alt="" />
								</div>
								<div class="single_home_content_title">
									<h4>Fly Fishers Serving as Transports for Noxious Little Invaders</h4>
									<p>August 4 2010, <a href="">8 Comments</a></p>
								</div>
							</div>
						</li>
						<li>
							<div class="fix single_home_content">
								<div class="fix single_home_content_img">
									<img src="images/small_feature.png" alt="" />
								</div>
								<div class="single_home_content_title">
									<h4>Fly Fishers Serving as Transports for Noxious Little Invaders</h4>
									<p>August 4 2010, <a href="">8 Comments</a></p>
								</div>
							</div>
						</li>
						<li>
							<div class="fix single_home_content">
								<div class="fix single_home_content_img">
									<img src="images/small_feature.png" alt="" />
								</div>
								<div class="single_home_content_title">
									<h4>Fly Fishers Serving as Transports for Noxious Little Invaders</h4>
									<p>August 4 2010, <a href="">8 Comments</a></p>
								</div>
							</div>
						</li>
					</ul>

				</div>

				<div class="fix single_content_latest_post floatleft">
					<h2><span>Latest Posts</span></h2>
					<div class="fix single_latest_post">
						<div class="fix latest_post_img floatleft">
							<img src="images/blog_post_img.png" alt="" />
						</div>
						<div class="fix latest_post_title floatright">
							<h2><a href="">Silicon Valley Shows Signs of Dot-Com Frenzy</a></h2>
							<span>August 4 2010, <a href="">8 Comments</a></span>
							<p>Lorem ipsum ex vix illud nonummy, novum tation et his. At vix scripta patrioque scribentur novum tation et his ex vix illud nonummy...</p>
						</div>
					</div>
					<div class="fix single_latest_post">
						<div class="fix latest_post_img floatleft">
							<img src="images/blog_post_img.png" alt="" />
						</div>
						<div class="fix latest_post_title floatright">
							<h2><a href="">Silicon Valley Shows Signs of Dot-Com Frenzy</a></h2>
							<p>August 4 2010, <a href="">8 Comments</a></p>
							<p>Lorem ipsum ex vix illud nonummy, novum tation et his. At vix scripta patrioque scribentur novum tation et his ex vix illud nonummy...</p>
						</div>
					</div>
					<div class="fix single_latest_post">
						<div class="fix latest_post_img floatleft">
							<img src="images/blog_post_img.png" alt="" />
						</div>
						<div class="fix latest_post_title floatright">
							<h2><a href="">Silicon Valley Shows Signs of Dot-Com Frenzy</a></h2>
							<p>August 4 2010, <a href="">8 Comments</a></p>
							<p>Lorem ipsum ex vix illud nonummy, novum tation et his. At vix scripta patrioque scribentur novum tation et his ex vix illud nonummy...</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php include "includes/sidebar.php" ?>
		
	</div>
	<div class="fix scroll_to_top">
		<a href="#scroll_top" class="floatright">Back to Top </a>
	</div>
</div>
</div>
</section>

<?php include "includes/footer.php" ?>

