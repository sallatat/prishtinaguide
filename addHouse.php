<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">

  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>PrishtinaGuide - Add House</title>

  <!-- Bootstrap -->
  <!--<link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body onload="onLoad()">

<<<<<<< HEAD
=======
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="js/global.js"></script>
>>>>>>> origin/pg-01
    
<!-- Design -->
<div class="container" id="main">

	<div id="globalBtns" class="btn-group btn-group-sm pull-right" role="group" aria-label="...">
      <?php
        if(empty($_SESSION['USERNAME'])) {
    ?>
          <?
          ob_start();
          ?>
          <button id="adminBtn" type="button" class="btn btn-default btn_color" onclick="window.location='loginForm.php'"><span class="glyphicon glyphicon-user"></span><span style="color: black; margin-left: 5px">Login </span></button>
          <?
          echo ob_get_clean();
          ?>
    <?php
        }
        else {
    ?>    <?
          ob_start();
          ?>
          <button id="adminBtn" type="button" class="btn btn-default btn_color" onclick="window.location='admin_crud.php'"><span class="glyphicon glyphicon-user"></span><span style="color: limegreen; margin-left: 5px"> <?php echo $_SESSION["USERNAME"] ?> </span></button>
<<<<<<< HEAD
          <?
          echo ob_get_clean();
          ?>
    <?php
        }
    ?>

    <?php
        if(!empty($_SESSION['USERNAME'])) {
    ?>
          <?
          ob_start();
          ?>
          <button id="logoutBtn" type="button" onclick="window.location='loginForm.php'" class="btn btn-default btn_color"><span style="color: red">Logout </span></button>
=======
          <button id="logoutBtn" type="button" class="btn btn-default btn_color"><span style="color: red">Logout </span></button>
>>>>>>> origin/pg-01
          <?
          echo ob_get_clean();
          ?>
    <?php
        }
    ?>
      <button id="usersBtn" type="button" onclick="window.location='users.php'" class="btn btn-default btn_color">Users</button>
      <button id="hotels" type="button" onclick="window.location='addHotel.php'" class="btn btn-default btn_color">Hotels</button>
      <button id="houses" type="button" onclick="window.location='addHouse.php'" class="btn btn-default btn_color">Houses</button>
    </div>

    <h2>Add House</h2>
    <hr>
    
<div class="row">
    
<form>
<div class="col-sm-4">
    <div id="owner-div" class="form-group form-group-sm">
        <label for="owner_input" >Owner</label>
        <input type="text" class="form-control" id="owner_input" placeholder="Required" required>
    </div>
    <div id="address-div" class="form-group form-group-sm">
      <label for="address_input">Address</label>
      <input type="text" class="form-control" id="address_input" placeholder="Required" required>
    </div>
    <div id="phone-div" class="form-group form-group-sm">
      <label for="phone_input">Phone</label>
      <input type="number" class="form-control" id="phone_input" placeholder="Required" required>
    </div>
</div>

<div class="col-sm-4">
    <div class="form-group form-group-sm">
      <label for="desc_input">Description</label>
      <textarea class="form-control" rows="9" id="desc_input" placeholder="Optional"></textarea>
    </div>
</div>

<div class="col-sm-4">
    <div id="price-div" class="form-group form-group-sm">
      <label for="price_input">Price/night</label>
      <input type="number" class="form-control" id="price_input" pattern="[0-9]+([\.,][0-9]+)?" step="0.01" placeholder="Required" required>
    </div>

    <button style="position:relative; top:94px;" type="submit" onclick="validate_addForm()" class="btn btn-success btn-block">Save</button>
</div>

<!-- Bootstrap Table -->
<div style="top: 30px;" class="col-sm-12">
<hr>
<h3>Table</h3>
<table class="table table-bordered table-striped table-condensed table-sm">
    <thead class="table-inverse">
        <tr>
            <th width="100">Owner</th>
            <th width="100">Address</th>
            <th width="180">Description</th>
            <th width="100">Phone</th>
            <th width="60">Price</th>
            <th width="130">Action</th>
        </tr>
    </thead>

    <tbody>
    </tbody>
</table>
</div>
    
</div>
</div>
</form>
    
<!-- Methods -->
<script type='text/javascript'>

function onLoad() {
    $('#houses').addClass('active');
    $('#houses').addClass('btn-primary');
}

function radioBtn(btn) {

    if(btn === 'adminBtn' && !(window.location === 'admin_crud.php')) {
        window.location="admin_crud.php";
    }
    if(btn === 'usersBtn' && !(window.location === 'users.php')) {
        window.location="users.php";
    }
    if(btn === 'hotels' && !(window.location === 'addHotel.php')) {
        window.location="addHotel.php";
    }
    if(btn === 'houses' && !window.location === 'addHouse.php') {
        window.location="addHouse.php";
    }
}

function saveData() {
    var owner = $('#owner_input').val();
    var address = $('#address_input').val();
    var desc = $('#desc_input').val();
    var phone = $('#phone_input').val();
    var price = $('#price_input').val();
    $.ajax({
        type: "POST",
        url: "house_Server.php?p=add",
        data: "owner_input="+owner+"&address_input="+address+"&desc_input="+desc+"&phone_input="+phone+"&price_input="+price,
        success: function(data){
            viewData();
        }
    });
}

function viewData() {
    $.ajax({
        type: "GET",
        url: "house_Server.php",
        success: function(data){
            $('tbody').html(data);
        }
    });
}

function updateData(str) {
    var id = str;
    var owner = $('#owner_input-'+str).val();
    var address = $('#address_input-'+str).val();
    var desc = $('#desc_input-'+str).val();
    var phone = $('#phone_input-'+str).val();
    var price = $('#price_input-'+str).val();
    $.ajax({
        type: "POST",
        url: "house_Server.php?p=edit",
        data: "owner_input="+owner+"&address_input="+address+"&desc_input="+desc+"&phone_input="+phone+"&price_input="+price+"&id="+id,
        success: function(data) {
            viewData();
        }
    });
}

function deleteData(str) {
    var id = str;
    $.ajax({
        type: "GET",
        url: "house_Server.php?p=del",
        data: "id="+id,
        success: function(data) {
            viewData();
        }
    });
    
}

function validate_addForm() {
    
    var owner = $('#owner_input').val();
    var address = $('#address_input').val();
    var phone = $('#phone_input').val();
    var price = $('#price_input').val();
    
    var ow_div = $('#owner_div');
    var ad_div = $('#address_div');
    var ph_div = $('#phone_div');
    var pr_div = $('#price_div');
    
    var ownerField = owner === null || owner === "";
    var addressField = address === null || address === "";
    var phoneField = phone === null || phone === "";
    var priceField = price === null || price === "";
    
    if(ownerField || addressField || phoneField || priceField) {
        if(ownerField) ow_div.addClass("has-error");
        if(addressField) ad_div.addClass("has-error");
        if(phoneField) ph_div.addClass("has-error");
        if(priceField) pr_div.addClass("has-error");
    }
    else {
        saveData();
    }
}

</script>
<<<<<<< HEAD

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="js/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.min.js"></script>
=======
>>>>>>> origin/pg-01
</body>
</html>