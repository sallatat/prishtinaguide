<!DOCTYPE html>
<html>

<head>
	<title>Prishtina Guide</title>
	<link rel="icon" type="image/gif/png" href="images/prishtina-logo.png">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="css/font-awesome.min.css"/>
	<link rel="stylesheet" type="text/css" href="css/w3.css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="fonts/HelveticaNeue/font.css">
	<link href="style.css" rel="stylesheet" media="screen">
	<link href="responsive.css" rel="stylesheet" media="screen">
	<link href="http://openweathermap.org/themes/openweathermap/assets/vendor/owm/css/openweathermap-widget-left.min.css" rel="stylesheet">
</head>

<body id="scroll_top">

	<section id="header_area">

		<div class="header_top_area">
			<div class="header_top center">
				<div class="header_left">
					<span class="site_date" id="clockbox">&#9830;</span>
					<nav>
						<ul id="nav" class="nav_right">
							<li><a href="">Add Event</a></li>
							<li><a href="">Add Place</a></li>
							<li><a href="">Contact Us</a></li>
							<li><a href="" class="down">Registration</a>
								<ul>
									<li><a href="" data-toggle="modal" data-target="#signInModal">Sign In</a></li>
									<div class="modal fade" tabindex="-1" id="signInModal" data-keyboard="false" data-backdrop="static">
									<div class="modal-dialog modal-md">
										<div class="modal-content" style="color:#333333;"> 
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">
													&times;
												</button>
												<h5 class="modal-title">Sign In</h5>
											</div>
											<div class="modal-body">
												<form>
													<div class="form-group">
														<label for="inputUserName">Username</label>
														<input class="form-control" placeholder="Username" type="text" id="inputUserName" />
													</div>
													<div class="form-group">
														<label for="inputPassword">Password</label>
														<input class="form-control" placeholder="Password" type="password" id="inputPassword" />
													</div>
												</form>
											</div>
											<div class="modal-footer">
												<button type="submit" class="btn btn-primary">Sign in</button>
												<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>

								<li><a href="" data-toggle="modal" data-target="#signUpModal">Sign Up</a></li>
								<div class="modal fade" tabindex="-1" id="signUpModal" data-keyboard="false" data-backdrop="static">
									<div class="modal-dialog modal-md">
										<div class="modal-content" style="color:#333333;">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">
													&times;
												</button>
												<h5 class="modal-title">Sign Up</h5>
											</div>
											<div class="modal-body">
												<form>
													<div class="form-group">
														<label for="first_name">First Name</label>
														<input class="form-control" placeholder="first name" type="text" id="first_name" />
													</div>
													<div class="form-group">
														<label for="last_mame">Last Name</label>
														<input class="form-control" placeholder="last name" type="text" id="last_name" />
													</div>
													<div class="form-group">
														<label for="user_name">Username</label>
														<input class="form-control" placeholder="username" type="text" id="user_name" />
													</div>
													<div class="form-group">
														<label for="password">Password</label>
														<input class="form-control" placeholder="password" type="password" id="password" />
													</div>
													<div class="form-group">
														<label for="age">Age</label>
														<input class="form-control" placeholder="" type="number" id="age" />
													</div>
													<div class="form-group">
														<label for="email">Email</label>
														<input class="form-control" placeholder="email" type="email" id="email" />
													</div>
													<div class="form-group">
														<label for="gender">Gender</label>
														<input class="form-control" placeholder="" type="radio" id="gender" />
														<input class="form-control" placeholder="" type="radio" id="gender" />
														<input class="form-control" placeholder="" type="radio" id="gender" />
													</div>

												</form>
											</div>
											<div class="modal-footer">
												<button type="submit" class="btn btn-primary">Sign Up</button>
												<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
							</ul>
						</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>


	<div class="fix header_bottom_area">
		<div class="header_bottom center">
			<div class="fix logo floatleft">
				<h1><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Prishtina Guide</h1>
			</div>
		</div>
	</div>
</section>
<section id="header_bottom_area"></section>
<section id="content_area">
	<div class="content center">
		<div class="main_menu">
			<nav>
				<ul id="nav2">
					<li><a href="">Accomodation <span>Hotels & Rent Houses</span></a>
						<ul>
							<li><a href="">Hotel</a></li>
							<li><a href="">Rent House</a></li>
						</ul>
					</li>
					<li><a href="">Businesses <span>Businesses Operating In Prishtina</span></a>
					</li>
					<li><a href="">Explore <span>Most Visited Places</span></a> </li>
					<li><a href="">Events <span>Night & Day Events</span></a> </li>
					<li><a href="">Transport <span>Info About Busses, Taxis & Trains</span></a>
						<ul>
							<li><a href="">Bus</a></li>
							<li><a href="">Taxi</a></li>
							<li><a href="">Train</a></li>
						</ul>
					</li>
					<li><a href="">Travel News <span>Travel News Guides & Tips</span></a></li>
				</ul>
			</nav>
		</div>