<<<<<<< HEAD
function onLoad() {
=======
function load() {
>>>>>>> origin/pg-01
    $('#usersBtn').addClass('active');
    $('#usersBtn').addClass('btn-primary');
    fixButtons();
    viewData();
<<<<<<< HEAD

    
=======
>>>>>>> origin/pg-01
}

var selectedRole = 2;

function adminBtnCheck() {
    $('#option2').removeClass("btn-warning");
    $('#option2').addClass("btn-secondary");
    $('#option1').addClass("btn-warning");
    selectedRole = $("#adm").val();
}

function usersBtnCheck() {
    $('#option1').removeClass("btn-warning");
    $('#option1').addClass("btn-secondary");
    $('#option2').addClass("btn-warning");
    selectedRole = $("#usr").val();
}

function cancelButton() {
    cleanFields();
    fixButtons();
}

function isValidStage_1(str) {
    var validStage_1 = true;
    //STAGE 1:
    //it checks the fields if they are filled or not...
    var arr = {fName:'fName', lName:'lName', username:'username', 
               password:'password', age:'age', email:'email', phone:'phone'};
    
    for(var x in arr) {
        var y = $('#'+x).val();
        if(y === null || y === ""){
            $('#'+x).closest('div').addClass("has-error");
            validStage_1 = false;
        }
        else {
            $('#'+x).closest('div').removeClass("has-error");
        }
    }

    if(validStage_1) {
        if(str == 'save') {
            isValidStage_2();
        }
        else {
            if(confirm("Do you want to update this data?")) updateData();
        }
    }
    else {
        alert("Not valid, please fill out fields correctly!");
    }
}

function isValidStage_2() {
    
    //STAGE 2:
    //IMPORTANT: will start on this stage only if we passed stage 1
    //it checks for same usernames...
    var username = $('#username').val();

    $.ajax({
    type: "POST",
    url: "Server/users_Server.php?p=checkUsername",
    data: "user="+username,
        success: function(data) {
            if(data == 'null') {
                saveData();
            }
            else {
                error();
            }
        }
    });
}

function error() {
    $('#usernameDiv').addClass('has-error');
    $('.control-label').css("visibility", "visible");
}

function saveData() {
    var fName = $('#fName').val();
    var lName = $('#lName').val();
    var username = $('#username').val();
    var password = $('#password').val();
    var age = $('#age').val();
    var email = $('#email').val();
    var phone = $('#phone').val();
    var role = selectedRole;
    $.ajax({
        type: "POST",
        url: "Server/users_Server.php?p=add",
        data: "fName="+fName+"&lName="+lName+"&username="+username+
        "&password="+password+"&age="+age+"&email="+email+
        "&phone="+phone+"&role="+role,
        success: function(data){
            viewData();
        }
    });
    cleanFields();
}

function cleanFields() {
    $('#fName').val("");
    $('#lName').val("");
    $('#username').val("");
    $('#password').val("");
    $('#age').val("");
    $('#email').val("");
    $('#phone').val("");
    $('.control-label').css("visibility", "hidden");
}

function viewData() {
    $.ajax({
        type: "GET",
        url: "Server/users_Server.php",
        success: function(data){
            $('tbody').html(data);
        }
    });
}

var ID;
function editMode(id) {
    $('#updateBtn').prop( "disabled", false );
    $('#cancelBtn').prop( "disabled", false );
    $('#saveBtn').prop( "disabled", true );
    $('#option1').addClass("disabled");
    $('#option2').addClass("disabled");

    $.ajax({
        type: "POST",
        url: "Server/users_Server.php?p=select_id",
        data: "id="+id,
        success: function(data) {
            setDataToEdit(data);
        }
    });

    ID = id;
}

function setDataToEdit(data) {
    var arr = data.split(" || ");

    $('#fName').val(arr[0]);
    $('#lName').val(arr[1]);
    $('#username').val(arr[2]);
    $('#password').val(arr[3]);
    $('#email').val(arr[4]);
    $('#age').val(arr[5]);
    $('#phone').val(arr[6]);
    var role = arr[7];
    if(role == 1) adminBtnCheck();
    else usersBtnCheck();
}

function updateData() {
    var fName = $('#fName').val();
    var lName = $('#lName').val();
    var username = $('#username').val();
    var password = $('#password').val();
    var email = $('#email').val();
    var age = $('#age').val();
    var phone = $('#phone').val();
    var role = selectedRole;
    var id = ID;
    $.ajax({
        type: "POST",
        url: "Server/users_Server.php?p=edit",
        data: "id="+id+"&fName="+fName+"&lName="+lName+"&username="+username+
        "&password="+password+"&age="+age+"&email="+email+"&phone="+phone+"&role="+role,
        success: function(data) {
            viewData();
        }
    });
    cleanFields();
    fixButtons();
}

function fixButtons() {
    $('#updateBtn').prop( "disabled", true );
    $('#cancelBtn').prop( "disabled", true );
    $('#saveBtn').prop( "disabled", false );
    $('#option1').removeClass("disabled");
    $('#option2').removeClass("disabled");
    usersBtnCheck();
}

function deleteData(str) {
    if(confirm("IMPORTANT: This data cannot be restored. Do you want to delete this record?!")) {
        $.ajax({
            type: "GET",
            url: "Server/users_Server.php?p=del",
            data: "id="+str,
            success: function(data) {
                viewData();
            }
        });
    }
}

function searchBox() {
    var value = $("#search").val().toUpperCase();

    $("table tr").each(function(index) {
        if (index !== 0) {

            $row = $(this);

            var id = $row.find("td:first").text().toUpperCase();

            if (id.indexOf(value) !== 0) {
                $row.hide();
            }
            else {
                $row.show();
            }
        }
    });
}

// Here is javascript code for loginForm.php:
// Here is javascript code for loginForm.php:
// Here is javascript code for loginForm.php:

function login() {
    var username = $('#usernameLogin').val();
    var password = $('#passwordLogin').val();

    $.ajax({
        type: "POST",
        url: "Server/users_Server.php?p=isReg",
        data: "user="+username+"&pass="+password,
        success: function(data) {
            isUserRegistered(data);
        }
    });
}

function isUserRegistered(data) {
    if(data === 'null') {
        $('#usernameLogin').css('border-color', 'red');
        $('#passwordLogin').css('border-color', 'red');
        alert("Incorrect username or password!");
    }

    else {
        window.location="users.php";
    }
    
}

$(function() {

    var a = $('#usernameLogin');
    var b = $('#passwordLogin');

    a.keypress(function(e)
    {
        if (e.keyCode == 13)
            login();
    });

    b.keypress(function(e)
    {
        if (e.keyCode == 13) 
            login();
    });
<<<<<<< HEAD

=======
>>>>>>> origin/pg-01
});