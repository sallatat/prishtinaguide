<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">

  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>PrishtinaGuide - Admin</title>

  <!-- Bootstrap -->
  <!--<link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
</head>
<body onload="onLoad()">
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="js/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.min.js"></script>
  <script src="js/admin.js"></script>
    
<!-- Design -->
<div class="container" id="main">
  
  <div id="globalBtns" class="btn-group btn-group-sm pull-right" role="group" aria-label="...">
    <?php
        if(empty($_SESSION['USERNAME'])) {
    ?>
          <?
          ob_start();
          ?>
          <button id="adminBtn" type="button" class="btn btn-default btn_color" onclick="window.location='loginForm.php'"><span class="glyphicon glyphicon-user"></span><span style="color: black; margin-left: 5px">Login </span></button>
          <?
          echo ob_get_clean();
          ?>
    <?php
        }
        else {
    ?>    <?
          ob_start();
          ?>
          <button id="adminBtn" type="button" class="btn btn-default btn_color" onclick="window.location='admin_crud.php'"><span class="glyphicon glyphicon-user"></span><span style="color: limegreen; margin-left: 5px"> <?php echo $_SESSION["USERNAME"] ?> </span></button>
<<<<<<< HEAD
          <?
          echo ob_get_clean();
          ?>
    <?php
        }
    ?>

    <?php
        if(!empty($_SESSION['USERNAME'])) {
    ?>
          <?
          ob_start();
          ?>
=======
>>>>>>> origin/pg-01
          <button id="logoutBtn" type="button" onclick="window.location='loginForm.php'" class="btn btn-default btn_color"><span style="color: red">Logout </span></button>
          <?
          echo ob_get_clean();
          ?>
    <?php
        }
    ?>
    <button id="usersBtn" type="button" onclick="window.location='users.php'" class="btn btn-default btn_color">Users</button>
    <button id="hotels" type="button" onclick="window.location='addHotel.php'" class="btn btn-default btn_color">Hotels</button>
    <button id="houses" type="button" onclick="window.location='addHouse.php'" class="btn btn-default btn_color">Houses</button>
  </div>
	
	<h3 style="font-weight: normal">Administrator: <span style="color: limegreen; font-weight: bold"><?php echo $_SESSION['USERNAME']; ?></span></h3>
	
	<hr>
    
</div>
</body>
</html>