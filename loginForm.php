<?php 
    session_start();
    session_destroy();

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/login.css">
    
    <title>PrishtinaGuide - LogIn</title>
</head>

<body>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="js/users.js"></script>

<div class = "container">
    <div class="wrapper">
        <form action="" method="post" name="Login_Form" class="form-signin">
            <h1 class="form-signin-heading"><span style="color: #337ab7">Prishtina Guide</span></h1>
            <label class="pos"><span style="color: #337ab7">Please Sign In!</span></label>
            <hr class="colorgraph"><br>

            <input id="usernameLogin" type="text" class="form-control" name="Username" placeholder="Username" required autofocus="" >
            <input id="passwordLogin" type="password" class="form-control" name="Password" placeholder="Password" required>  

            <button id="btnLogin" onclick="login()" class="btn btn-lg btn-primary btn-block" name="Submit" type="button">Login</button>
            
            <a class="pull-right" href="#">Register</a>
            <a class="pull-left" href="#">Forget password</a>
        </form>
    </div>
</div>
</body>
</html>