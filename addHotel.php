<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">

  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>PrishtinaGuide - Add Hotel</title>

  <!-- Bootstrap -->
  <!--<link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body onload="onLoad()">

<<<<<<< HEAD
=======
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/global.js"></script>
>>>>>>> origin/pg-01

<!-- Design -->
<div class="container" id="main">

	<div id="globalBtns" class="btn-group btn-group-sm pull-right" role="group" aria-label="...">
      <?php
        if(empty($_SESSION['USERNAME'])) {
    ?>
          <?
          ob_start();
          ?>
          <button id="adminBtn" type="button" class="btn btn-default btn_color" onclick="window.location='loginForm.php'"><span class="glyphicon glyphicon-user"></span><span style="color: black; margin-left: 5px">Login </span></button>
          <?
          echo ob_get_clean();
          ?>
    <?php
        }
        else {
    ?>    <?
          ob_start();
          ?>
          <button id="adminBtn" type="button" class="btn btn-default btn_color" onclick="window.location='admin_crud.php'"><span class="glyphicon glyphicon-user"></span><span style="color: limegreen; margin-left: 5px"> <?php echo $_SESSION["USERNAME"] ?> </span></button>
<<<<<<< HEAD
          <?
          echo ob_get_clean();
          ?>
    <?php
        }
    ?>

    <?php
        if(!empty($_SESSION['USERNAME'])) {
    ?>
          <?
          ob_start();
          ?>
          <button id="logoutBtn" type="button" onclick="window.location='loginForm.php'" class="btn btn-default btn_color"><span style="color: red">Logout </span></button>
=======
          <button id="logoutBtn" type="button" class="btn btn-default btn_color"><span style="color: red">Logout </span></button>
>>>>>>> origin/pg-01
          <?
          echo ob_get_clean();
          ?>
    <?php
        }
    ?>
    <button id="usersBtn" type="button" onclick="window.location='users.php'" class="btn btn-default btn_color">Users</button>
      <button id="hotels" type="button" onclick="window.location='addHotel.php'" class="btn btn-default btn_color">Hotels</button>
      <button id="houses" type="button" onclick="window.location='addHouse.php'" class="btn btn-default btn_color">Houses</button>
    </div>
    
    <h2>Add Hotel</h2>
    <hr>
    
<div class="row">
    
<form>
<div class="col-sm-4">
    <div id="ht-div" class="form-group form-group-sm">
        <label for="ht" >Hotel</label>
        <input type="text" class="form-control" id="ht" placeholder="Required" required>
    </div>
    <div id="ad-div" class="form-group form-group-sm">
      <label for="ad">Address</label>
      <input type="text" class="form-control" id="ad" placeholder="Required" required>
    </div>
    <div id="ph-div" class="form-group form-group-sm">
      <label for="ph">Phone</label>
      <input type="number" class="form-control" id="ph" placeholder="Required" required>
    </div>
</div>

<div class="col-sm-4">
	<div class="form-group form-group-sm">
      <label for="de">Description</label>
      <textarea class="form-control" rows="9" id="de" placeholder="Optional"></textarea>
    </div>
</div>

<div class="col-sm-4">    
    <div id="price-div" class="form-group form-group-sm">
      <label for="price">Price/night</label>
      <input type="number" class="form-control" id="price" placeholder="Required" required>
    </div>
    
    <div class="form-group form-group-sm">
        <label for="rate_btn">Rate </label>
        <select id="rate_btn" style="outline: 1px auto -webkit-focus-ring-color;" class="selectpicker btn btn-sm btn-secondary pull-right ">
            <option selected value="0">- -</option>
            <option style="font-size: 14px;" value="1">1.0</option>
            <option style="font-size: 14px;" value="2">2.0</option>
            <option style="font-size: 14px;" value="3">3.0</option>
            <option style="font-size: 14px;" value="4">4.0</option>
            <option style="font-size: 14px;" value="5">5.0</option>
        </select>
    </div>
    
    <button style="position:relative; top:55px;" type="submit" onclick="validate_addForm()" class="btn btn-success  btn-block">Save</button>
</div>

<!-- Bootstrap Table -->

<div style="top: 30px;" class="col-sm-12">
<hr>
<h3>Table</h3>
<table class="table table-bordered table-striped table-condensed table-sm">
    <thead class="table-inverse">
        <tr>
            <th width="100">Hotel</th>
            <th width="100">Address</th>
            <th width="180">Description</th>
            <th width="100">Phone</th>
            <th width="100">Price</th>
            <th width="60">Rate</th>
            <th width="130">Action</th>
        </tr>
    </thead>

    <tbody>
    </tbody>
</table>
</div>
    
</div>
</div>
</form>
    
<!-- Methods -->
<script type='text/javascript'>

function onLoad() {
    $('#hotels').addClass('active');
    $('#hotels').addClass('btn-primary');
}

function radioBtn(btn) {

    if(btn === 'adminBtn' && !(window.location === 'admin_crud.php')) {
        window.location="admin_crud.php";
    }
    if(btn === 'usersBtn' && !(window.location === 'users.php')) {
        window.location="users.php";
    }
    if(btn === 'hotels' && !window.location === 'addHotel.php') {
        window.location="addHotel.php";
    }
    if(btn === 'houses' && !(window.location === 'addHouse.php')) {
        window.location="addHouse.php";
    }
}

function saveData() {
    var hotel = $('#ht').val();
    var address = $('#ad').val();
    var desc = $('#de').val();
    var phone = $('#ph').val();
    var rate_btn = $('#rate_btn').val();
    var price = $('#price').val();
    $.ajax({
        type: "POST",
        url: "hotel_Server.php?p=add",
        data: "ht="+hotel+"&ad="+address+"&de="+desc+"&price="+price+"&ph="+phone+"&rate_btn="+rate_btn,
        success: function(data){
            viewData();
        }
    });
}

function viewData() {
    $.ajax({
        type: "GET",
        url: "hotel_Server.php",
        success: function(data){
            $('tbody').html(data);
        }
    });
}

function updateData(str) {
    var id = str;
    var hotel = $('#ht-'+str).val();
    var address = $('#ad-'+str).val();
    var desc = $('#de-'+str).val();
    var phone = $('#ph-'+str).val();
    var rate_btn = $('#rate_btn-'+str).val();
    //alert("ht: "+hotel+" ad: "+address+" de: "+desc+" ph: "+phone+" rate: "+rate_btn);
    $.ajax({
        type: "POST",
        url: "hotel_Server.php?p=edit",
        data: "ht="+hotel+"&ad="+address+"&de="+desc+"&ph="+phone+"&rate_btn="+rate_btn+"&id="+id,
        success: function(data) {
            viewData();
        }
    });
}

function deleteData(str) {
    var id = str;
    $.ajax({
        type: "GET",
        url: "hotel_Server.php?p=del",
        data: "id="+id,
        success: function(data) {
            viewData();
        }
    });
    
}

function validate_addForm() {
    
    var hotel = $('#ht').val();
    var address = $('#ad').val();
    var phone = $('#ph').val();
    
    var ht_div = $('#ht-div');
    var ad_div = $('#ad-div');
    var ph_div = $('#ph-div');
    
    var hotelField = hotel === null || hotel === "";
    var addressField = address === null || address === "";
    var phoneField = phone === null || phone === "";
    
    if(hotelField || addressField || phoneField) {
        if(hotelField) ht_div.addClass("has-error");
        if(addressField) ad_div.addClass("has-error");
        if(phoneField) ph_div.addClass("has-error");
    }
    else {
        saveData();
    }
}

</script>
<<<<<<< HEAD

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="js/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.min.js"></script>
=======
>>>>>>> origin/pg-01
</body>
</html>